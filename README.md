# jwtsessions

使用 gorilla/sessions，看了底层源码使用的不习惯，自己改造一个

### 使用方式

```golang

session := jwtsession.AutoSession(ctx, s.store, httpTransport.Request(), "session", jwtsession.WithInspector(jwtsession.HeaderInspector{}))


// 保存
session.Save(ctx, func(ctx context.Context, name, value string) error {
		w.Header().Set(name, value)
		return nil
})


```

### http server 下使用
