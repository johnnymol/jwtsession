package biz

import (
	"log"

	"gitee.com/johnnymol/jwtsession"
	"github.com/redis/go-redis/v9"
)

func NewJwtRedisStore(rdCmd redis.Cmdable) *jwtsession.RedisStore {

	store, err := jwtsession.NewRedisStore(rdCmd, "jwt_session_")
	if err != nil {
		log.Fatalf("new sessionStroe error: %v", err)
	}
	return store
}
