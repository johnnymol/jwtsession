package jwtsession

import (
	"context"
	"net/http"
)

type Options struct {
	Path      string
	Domain    string
	MaxAge    int
	Secure    bool
	HttpOnly  bool
	SameSite  http.SameSite
	Inspector Inspector
}

type WritterFunc func(ctx context.Context, name, value string) error

type HandlerOption func(opt *Options)

// func WithWritter(fun WritterFunc) HandlerOption {
// 	return func(opt *Options) {
// 		opt.Writter = fun
// 	}
// }

func WithInspector(inspector Inspector) HandlerOption {
	return func(opt *Options) {
		opt.Inspector = inspector
	}
}

func WithMaxAge(maxAge int) HandlerOption {
	return func(opt *Options) {
		opt.MaxAge = maxAge
	}
}
