package jwtsession

import "context"

// Store is an interface for custom session stores.
//
// See CookieStore and FilesystemStore for examples.
type Store interface {
	// Get should return a cached session.
	Get(ctx context.Context, sessionId string) (map[string]string, error)

	// Save should persist session to the underlying store implementation.
	Save(ctx context.Context, sessionId string, values map[string]string, opts *Options) error
}
